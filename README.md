# StockMaster

The StockMaster project is an application designed for inventory management.

## Project Overview

StockMaster is developed in the C# programming language using the .NET framework. The application provides the ability to view product information, manage product data and images, and display additional product information.

## Functionality

- **View Product Data:** Access information about products including type, brand, price, color, and size.
- **Image Management:** Upload and view product images within the application.
- **Material and Care Information Display:** Provides details about materials used and recommendations for product care.


## Technologies Used

- **Programming Language:** C#
- **Libraries Used:** DevExpress, System.Data.SqlClient
- **Database Management System:** Microsoft SQL Server

## Usage Instructions

1. **Clone Repository:** Clone the project repository to your local machine.
2. **Open Project:** Open the project in Visual Studio.
3. **Install Dependencies:** Ensure all required dependencies are installed and connected to the project.
4. **Database Connection Setup:** Modify the database connection string in the `App.config` file as needed. Find the line **connectionString="Data Source=DESKTOP-XXXXXXX;Initial Catalog=StockMasterDBMS;Integrated Security=True**, and replace **DESKTOP-XXXXXXX** with the name of the server where your database is located.
5. **Run Application:** Launch the application in Visual Studio to begin usage.

## Configuring the database

To create a database and fill tables, use the provided SQL scripts.

### Steps to set up the database:
1. **Creating a database and tables:** Run the SQL script, for example in SSMS, `SQLQuery_create DB&tables.sql`, to create a database and the necessary tables.
2. **Filling tables with data:** Run SQL script `SQLQuery_insert into product_db&material_care.sql` to fill tables with data about products and care materials.
3. **Filling the table with images (optional):** Product images are included in the **images_shoes** directory within the project. This step is optional and can be performed at your discretion. Open the `SQLQuery_insert into product_images.sql` script and before running it, change the path in the script code from '**D:\images_shoes**' to where your **images_shoes** folder is located. Now the script is ready to run.

## How it works

When the application is launched, a welcome window appears first, followed by the main screen with a menu panel. The `Katalóg` button is active; clicking on it opens a window displaying a table of products. Clicking on the name of any product reveals additional information about the product and displays its image at the bottom.
   
***

# Author

The project is developed and maintained by `svetlanaObra`.