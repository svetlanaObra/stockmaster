USE StockMasterDBMS;

INSERT INTO StockSchema.product_db (type_of, label_of, price, color, size) 
VALUES
('Sneakersy KL62530 Lthr', 'KARL LAGERFELD', 149.00, 'white', 38),
('Top�nky adidas Superstar J GW4062', 'adidas', 52.00, 'white', 36),
('Sneakersy WL574EVW', 'New Balance', 103.00, 'B�ov�', 38),
('Loafers FELIX-222479', 'Gino Rossi', 64.99, '�ierna', 38),
('Loafers Carnaby 9422109109', 'Kurt Geiger', 109.00, 'Black', 36),
('Loafers Alex W 5048-301-20', 'Vagabond', 110.00, 'Black', 37),
('Oxfordky VALENCIA-107725', 'Simple', 99.99, 'B�ov�', 38),
('Oxfordky Tamaris 1-23204-41', 'Tamaris', 56.00, 'Black', 36),
('Gum�ky Lady Wenlock RWF667', 'Regatta', 24.00, 'Icegry/Slate', 38),
('Gum�ky Dry Walker Strack 107/36', 'Dry Walker', 31.00, 'Yellow', 38);
GO

INSERT INTO StockSchema.material_care (product_id, material_care_info) 
VALUES 
(1, 'Vlo�ka: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
             'Vn�tro: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�) - L�cova' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Ozdoby: Syntetika'),

(2, 'Vlo�ka: Textil' + CHAR(13) + CHAR(10) +
             'Vn�tro: Textil' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�) - L�cova, Pr�rodn� ko�a(use�)/-Pokryt� in�m materi�lom' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Obsahuje netextiln� �asti �ivo��neho p�vodu: �no'),

(3, 'Vlo�ka: Ko�a' + CHAR(13) + CHAR(10) +
             'Vn�tro: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�) - L�cova' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Ozdoby: Syntetika'),

(4, 'Materi�l podp�tku: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Zateplenie: Nie' + CHAR(13) + CHAR(10) +
             'Vlo�ka: Ko�a' + CHAR(13) + CHAR(10) +
             'Vn�tro: Ko�a' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�)/-Pr�rodn� ko�a' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Obsahuje netextiln� �asti �ivo��neho p�vodu: �no' + CHAR(13) + CHAR(10) +
             'Ozdoby: Ozdoba nie je odn�mate�n�'),

(5, 'Vlo�ka: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
             'Vn�tro: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�) - L�cova' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Ozdoby: Zirk�n'),

(6, 'Materi�l podp�tku: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Vlo�ka: Ko�a' + CHAR(13) + CHAR(10) +
             'Vn�tro: Ko�a, Textil' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�) - L�cova' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l'),

(7, 'Vlo�ka: Ko�a' + CHAR(13) + CHAR(10) +
             'Vn�tro: Ko�a' + CHAR(13) + CHAR(10) +
             'Zvr�ok: Pr�rodn� ko�a(use�) - Lakovan�' + CHAR(13) + CHAR(10) +
             'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
             'Obsahuje netextiln� �asti �ivo��neho p�vodu: �no'),

(8, 'Vlo�ka: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
    'Vn�tro: Imit�cia ko�e' + CHAR(13) + CHAR(10) +
    'Zvr�ok: Imit�cia ko�e/-Imit�cia ko�e' + CHAR(13) + CHAR(10) +
    'Podr�ka: Vysokokvalitn� materi�l'),

(9, 'Materi�l podp�tku: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
    'Zateplenie: Nie' + CHAR(13) + CHAR(10) +
    'Vlo�ka: Textil' + CHAR(13) + CHAR(10) +
    'Vn�tro: Textil' + CHAR(13) + CHAR(10) +
    'Zvr�ok: Materi�l/-Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
    'Podr�ka: Vysokokvalitn� materi�l'),

(10, 'Zateplenie: �no' + CHAR(13) + CHAR(10) +
    'Vlo�ka: Filc' + CHAR(13) + CHAR(10) +
    'Vn�tro: Fleece' + CHAR(13) + CHAR(10) +
    'Zvr�ok: Materi�l/-Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
    'Podr�ka: Vysokokvalitn� materi�l' + CHAR(13) + CHAR(10) +
    'Technol�gie: EVA' + CHAR(13) + CHAR(10) +
    'Obsahuje netextiln� �asti �ivo��neho p�vodu: Nie');