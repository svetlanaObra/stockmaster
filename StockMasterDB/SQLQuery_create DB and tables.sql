-- Creating a database
CREATE DATABASE StockMasterDBMS;
GO

-- Using the database
USE StockMasterDBMS;
GO

-- Creating a user schema
CREATE SCHEMA StockSchema;
GO

-- Creating the product_db table in the user schema
CREATE TABLE StockSchema.product_db (
    id int IDENTITY(1,1) NOT NULL,
    type_of varchar(50) NOT NULL,
    label_of varchar(50) NOT NULL,
    price DECIMAL(7, 2) NOT NULL,
    color varchar(50) NOT NULL,
    size int NOT NULL,
    PRIMARY KEY (id)
);
GO

-- Creating the material_care table in the user schema
CREATE TABLE StockSchema.material_care (
    id int IDENTITY(1,1) NOT NULL,
    product_id int NOT NULL,
    material_care_info NVARCHAR(MAX) NOT NULL, 
    PRIMARY KEY (id),
    FOREIGN KEY (product_id) REFERENCES StockSchema.product_db(id) 
);
GO

-- Creating the product_images table in the user schema
CREATE TABLE StockSchema.product_images (
    id int IDENTITY(1,1) NOT NULL,
    product_id int NOT NULL,
    image_data VARBINARY(MAX), 
    PRIMARY KEY (id),
    FOREIGN KEY (product_id) REFERENCES StockSchema.product_db(id)
);