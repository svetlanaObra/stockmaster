USE StockMasterDBMS;

INSERT INTO StockSchema.product_images (product_id, image_data)
VALUES
(1, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\KarlLagerfeld.png', SINGLE_BLOB) AS image)),
(2, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\adidas.jpg', SINGLE_BLOB) AS image)),
(3, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\NewBalance.png', SINGLE_BLOB) AS image)),
(4, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\GinoRossi.png', SINGLE_BLOB) AS image)),
(5, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\KurtGeiger.png', SINGLE_BLOB) AS image)),
(6, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\Vagabond.png', SINGLE_BLOB) AS image)),
(7, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\Simple.png', SINGLE_BLOB) AS image)),
(8, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\Tamaris.png', SINGLE_BLOB) AS image)),
(9, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\Regata.png', SINGLE_BLOB) AS image)),
(10, (SELECT * FROM OPENROWSET(BULK N'D:\images_shoes\DryWalker.png', SINGLE_BLOB) AS image));