﻿using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockMaster
{
    public partial class frmMain : XtraForm
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public frmMain()
        {
            try
            {
                ShowSplashScreen();
                InitializeComponent();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            finally
            {
                CloseSplashScreen();
            }
        }

        private void ShowSplashScreen()
        {
            try
            {
                FluentSplashScreenOptions fluentSplashScreenOptions = new FluentSplashScreenOptions
                {
                    Title = "Vitajte v aplikácii!",
                    Subtitle = "Stock Master Software",
                    RightFooter = "Starting...",
                    LeftFooter = $"Copyright © 2023 {Environment.NewLine} All Right reserved",
                    LoadingIndicatorType = FluentLoadingIndicatorType.Dots,
                    OpacityColor = System.Drawing.Color.FromArgb(16, 110, 190),
                    Opacity = 90
                };

                fluentSplashScreenOptions.AppearanceLeftFooter.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                SplashScreenManager.ShowFluentSplashScreen(fluentSplashScreenOptions, parentForm: this, useFadeIn: true, useFadeOut: true);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CloseSplashScreen()
        {
            SplashScreenManager.CloseForm(false, 600, this);
        }

        private void barButtonKatalog_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ShowCatalogForm();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ShowCatalogForm()
        {
            frmGrid catalogForm = new frmGrid(connectionString);
            catalogForm.Show();
        }

        private void HandleException(Exception ex)
        {
            XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}