//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StockMaster
{
    using System;
    using System.Collections.Generic;
    
    public partial class product_images
    {
        public int id { get; set; }
        public int product_id { get; set; }
        public byte[] image_data { get; set; }
    
        public virtual product_db product_db { get; set; }
    }
}
