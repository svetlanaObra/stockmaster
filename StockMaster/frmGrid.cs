﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockMaster
{
    public partial class frmGrid : XtraForm
    {
        private readonly string connectionString;
        private DataTable dataTable;

        public frmGrid(string connectionString)
        {
            this.connectionString = connectionString;
            InitializeComponent();
            LoadDataFromDatabase();
            gridView1.RowCellClick += gridView1_RowCellClick;
            Load += frmGrid_Load;
        }

        private void frmGrid_Load(object sender, EventArgs e)
        {

        }

        private void LoadDataFromDatabase()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string query = "SELECT * FROM StockSchema.product_db";

                    using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                    {
                        dataTable = new DataTable();
                        adapter.Fill(dataTable);
                        gridControl1.DataSource = dataTable;

                        gridView1.Columns["type_of"].Caption = "Názov";
                        gridView1.Columns["label_of"].Caption = "Značka";
                        gridView1.Columns["price"].Caption = "Cena";
                        gridView1.Columns["color"].Caption = "Farba";
                        gridView1.Columns["size"].Caption = "Rozmer";                        

                        gridView1.OptionsBehavior.Editable = false;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public DataTable GetData()
        {
            return dataTable;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.RowHandle >= 0 && e.Column.FieldName == "type_of")
            {
                int productId = Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "id"));

                LoadMaterialCareData(productId);
                LoadImages(productId);
            }
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == xtraTabPageImage)
            {
                int productId = Convert.ToInt32(gridView1.GetFocusedRowCellValue("id"));

                LoadImages(productId);
            }
        }

        private void LoadMaterialCareData(int productId)
        {
            string query = $"SELECT material_care_info FROM StockSchema.material_care WHERE product_id = {productId}";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                    DataTable materialCareData = new DataTable();
                    adapter.Fill(materialCareData);
                    memoProductInformation.ReadOnly = true;


                    memoProductInformation.DataBindings.Clear();

                    memoProductInformation.Text = materialCareData.Rows[0]["material_care_info"].ToString();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private System.Drawing.Image ConvertBinaryToImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0)
                return null;

            using (MemoryStream ms = new MemoryStream(imageData))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }

        private void LoadImages(int productId)
        {
            string query = $"SELECT image_data FROM StockSchema.product_images WHERE product_id = {productId}";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                    DataTable imageData = new DataTable();
                    adapter.Fill(imageData);

                    if (imageData.Rows.Count > 0)
                    {
                        byte[] imageBytes = (byte[])imageData.Rows[0]["image_data"];
                        pictureEdit.Image = ConvertBinaryToImage(imageBytes);
                    }
                    else
                    {

                        pictureEdit.Image = null;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

}